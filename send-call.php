<?php

require "vendor/autoload.php";

$config = json_decode(file_get_contents('config.json'), true);

if(!$config['enabled']){
    return;
}

date_default_timezone_set('America/New_York');

//Check the day in the config
$day = strtolower(date('l'));
if(!in_array($day, $config['timing']['days'])){
    return;
}

$time = date('G');
if($time > $config['timing']['endTime'] || $time < $config['timing']['startTime']){
    return;
}


$client = new Services_Twilio('AC74a1d99d7733feccb8610d52d36542c3','TWILIO_AUTH_TOKEN');
try {
    // Place an outbound call
    $call = $client->account->calls->create(
        $config['twilio']['phoneNumber'], // A Twilio number in your account
        $_POST['number'], // The visitor's phone number
        $config['baseURL'].'outgoing.php'

    );
} catch (Exception $e) {
    echo "Failed!";
    return $e;
}

// return a JSON response
echo "Sending message";