# Installation

First install the twilio php sdk using composer.  Run the following command in the terminal at the root of your project
```sh
$ composer require twilio/sdk
```

Next add the following files in your project where you would like twilio to live.

1. config.json
2. send-call.php
3. outgoing.php
4. process.php

You will need to update the path of the require at the top of send-call.php to point to your vendor/autoload.php file.


# Configuration

Open the *config.json* file and update all the values.  

Update *send-call.php* with your Twilio auth token.

PLEASE NOTE: The welcome key in the messages array should be a link to an mp3 file.  There is a greeting file included if you would like to point to that.

PLEASE NOTE: the baseURL should be the base endpoint of the twilio portion of this application.  So if you want the app to hit the outgoing.php file at http://exampleapp.com/folder/outgoing.php the base URL would be http://exampleapp.com/folder/.  Be sure to include the trailing slash.


# How To Use

The send-call.php file will receive a POST request with the phone number to call set to the *number* key in the request.  That's it, everything will be handled by the application from there.

## Disabling/Enabling
You can disable the app by simply swapping the value of the *enabled* key in config.json to false.  To enable it, simply switch the value of *enabled* back to true.