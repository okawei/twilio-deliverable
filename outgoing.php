<?php

$config = json_decode(file_get_contents('config.json'), true);

// A message for Twilio's TTS engine to repeat
$sayMessage = $config['messages']['welcome'];

$twiml = '<?xml version="1.0" encoding="UTF-8"?>
            <Response>
               <Gather action="'.$config['baseURL'].'process.php" method="GET">
                    <Play loop="1">'.$config['messages']['welcome'].'</Play>
                </Gather>
            </Response>';
header('Content-Type: text/xml');
echo $twiml;
