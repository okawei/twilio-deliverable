<?php

$config = json_decode(file_get_contents('config.json'), true);

if($_REQUEST['Digits'] == 1){
    $content = '<?xml version="1.0" encoding="UTF-8"?>
            <Response>
              <Say>'.$config['messages']['accept'].'</Say>
              <Dial timeout="10">'.$config['redirectNumber'].'</Dial>

            </Response>';
}
else{
    $content = '<?xml version="1.0" encoding="UTF-8"?>
            <Response>
                <Say>
                    '.$config['messages']['deny'].'
                </Say>

            </Response>';
}


header('Content-Type: text/xml');
echo $content;
